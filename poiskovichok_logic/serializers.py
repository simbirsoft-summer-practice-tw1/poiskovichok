from rest_framework import serializers
from poiskovichok_logic.models import Product


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ('product_name', 'product_link', 'product_img_link', 'product_rub_price', 'vendor_name', 'counter')
