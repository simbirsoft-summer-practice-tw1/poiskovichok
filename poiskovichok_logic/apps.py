from django.apps import AppConfig


class PoiskovichokLogicConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'poiskovichok_logic'
