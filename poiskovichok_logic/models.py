from django.db import models


class Product(models.Model):
    product_name = models.CharField(max_length=256)
    product_link = models.CharField(max_length=256)
    product_img_link = models.CharField(max_length=50000, blank=True, null=True)
    product_rub_price = models.CharField(max_length=256)
    vendor_name = models.CharField(max_length=256)
    counter = models.IntegerField(default=1)
