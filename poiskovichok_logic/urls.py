from django.urls import path, include
from rest_framework import routers

from poiskovichok_logic import views

router = routers.DefaultRouter()
router.register('products', views.ProductViewSet, basename='products')
