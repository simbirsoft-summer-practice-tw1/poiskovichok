from bs4 import BeautifulSoup
from urllib.parse import urljoin
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from os.path import join
import time


def search(term, num_results=10, lang="en", product_sort='relevance'):
    usr_agent = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/61.0.3163.100 Safari/537.36'}

    def fetch_results(search_term, number_results, language_code):
        dict_with_sort = {'relevance': '',
                          'rate': ':rv',
                          'ascending': ':p',
                          'descending': ':pd'}

        escaped_search_term = search_term.replace(' ', '+')

        google_url = 'https://www.google.com/search?q={}&num={}&hl={}&tbm=shop&tbs=vw:l,p_ord{}'.format(
            escaped_search_term, number_results + 1,
            language_code, dict_with_sort[product_sort])

        options = Options()
        options.add_argument("--headless")

        driver = webdriver.Chrome(join('poiskovichok_logic', 'chromedriver'), options=options)
        driver.get(google_url)
        time.sleep(1)
        html = driver.page_source

        return html

    def shop_parse(raw_html):
        soup = BeautifulSoup(raw_html, 'html.parser')
        result_block = soup.find_all('div', attrs={'class': 'sh-dlr__list-result'})

        result_lst = []
        keys = ('product_name', 'product_link', 'product_img_link',
                'product_rub_price', 'vendor_name')

        for result in result_block:
            product_name = result.find('h3', attrs={'class': 'OzIAJc'}).text
            product_link = urljoin('https://www.google.com', result.find('a', href=True)['href'])
            product_img_link = result.find('div', attrs={'class': 'JRlvE XNeeld'}).find('img')['src']
            product_rub_price = result.find('span', attrs={'class': 'a8Pemb'}).text.split()[1]

            vendor_name = result.find('div', attrs={'class': 'b07ME mqQL1e'}).find('span').text

            result_dict = dict(zip(keys, (product_name, product_link,
                                          product_img_link, product_rub_price, vendor_name)))

            result_lst.append(result_dict)

        return result_lst

    html = fetch_results(term, num_results, lang)

    return shop_parse(html)
