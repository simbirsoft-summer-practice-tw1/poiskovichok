import json

from django.shortcuts import render
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.decorators import action

from poiskovichok_logic.models import Product
from poiskovichok_logic.serializers import ProductSerializer
from poiskovichok_logic.helpers import search

# Create your views here.


def index(request):
    return render(request, 'index.html', {})


class ProductViewSet(viewsets.ModelViewSet):

    queryset = Product.objects.all()
    serializer_class = ProductSerializer

    @action(methods=['POST'], detail=False, serializer_class=ProductSerializer)
    def method_a(self, request):
        print('отвалился')
        text_to_search = request.data.get('text_to_search')
        # categories = self.request.GET.get('categories', '').split(',') - not working

        result = search(text_to_search)
        print(result)
        json.dumps(result)

        return Response(result)

    @action(methods=['GET'], detail=False, serializer_class=ProductSerializer)
    def method_b(self, request):
        products = Product.objects.all()
        sorted_objects = sorted(list(products), key=lambda product: product.counter, reverse=True)

        result = []

        print_result = [x.counter for x in sorted_objects]
        print(print_result)

        keys = ('product_name', 'product_link', 'product_img_link',
                'product_rub_price', 'vendor_name')

        for object_ in sorted_objects:
            result_dict = dict(zip(keys, (object_.product_name, object_.product_link,
                                          object_.product_img_link, object_.product_rub_price, object_.vendor_name)))
            result.append(result_dict)

        return Response(result)  # should be sorted by count max 100

    @action(methods=['POST'], detail=False, serializer_class=ProductSerializer)
    def method_c(self, request):
        print('ОРАВЛЫРЛАРЫЛДАРЛЫРАРЫОЛРАЫРВАРЫРАОЛЫРОАРЫОЛРАЛЫВРЛАЫ')

        try:
            product = Product.objects.get(product_link=request.data.get('product_link'))
            product.counter += 1
        except Product.DoesNotExist:
            product = Product(
                product_name=request.data.get('product_name'),
                product_link=request.data.get('product_link'),
                product_img_link=request.data.get('product_img_link'),
                product_rub_price=request.data.get('product_rub_price'),
                vendor_name=request.data.get('vendor_name')
            )
        product.save()
        return Response()
