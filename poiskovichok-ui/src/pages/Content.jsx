import React from "react";
import icon1 from '../img/category_background_1.png'
import icon2 from '../img/category_background_2.png';
import icon3 from '../img/category_background_3.png';
import icon4 from '../img/category_background_4.png';
import icon5 from '../img/category_background_5.png';
import icon6 from '../img/category_background_6.png';
import CategoryElement from "../components/CategoryElement";
import Products from "../components/Products";
import s from "../components/Content.module.css";
import SearchingForm from "../components/SearchingForm";



class Content extends React.Component{
    constructor() {
        super();
        this.state ={
            data:'',
        }
    }
    handle(e)
    {
if (e.key === 'Enter')
        {
            this.setState({
                data:e.target.value
            })
            this.componentDidMount(this.state.data)
            console.log(e.target.value)
        }

    }
    //checking state of response
    state = {
        loading: true,
        items: null
    }
    //request products data from api
    async componentDidMount() {
        const searchText = this.state.data;
        if (searchText === "")
        {
            console.log("Method B");
            const apiUrl ="http://127.0.0.1:8000/api/products/method_b/";
            const response = await fetch(apiUrl);
            const data = await response.json();
            this.setState({items: data, loading:false});
            console.log(this.state.items);
        }
        else{
            console.log(" Method A ");
            const apiUrl ="http://127.0.0.1:8000/api/products/method_a/";
            console.log(searchText);
            const response = await fetch(
                apiUrl,
                {
                    method: "POST",
                    body: JSON.stringify({"text_to_search": searchText}),
                    headers: {"Content-type": "application/json"}
                }
            ).catch((e) => {console.log(e);})
            const data = await response.json();
            this.setState({items: data, loading:false});
        }
    }

    render() {
        return(
            <div>
                <div className={s.content}>
                    <table className={s.categories}>
 <tr className={s.category_row}>
                            <td className={s.category_element} onClick={() => {this.state.data = "Электроника"; console.log(this.state.data)}}
                                style={{backgroundImage: `url(${icon1})`, backgroundSize: "cover", marginLeft: "0",width: 100+"%"}}
                            >

                                <div className={s.category_element_name} >{"Электроника"} </div>
                            </td>
                            <td className={s.category_element} onClick={() => {this.state.data = "Продукты питания"; console.log(this.state.data)}}
                                style={{backgroundImage: `url(${icon2})`, backgroundSize: "cover", marginLeft: 48+"px"}}
                            >

                                <div className={s.category_element_name}>{"Продукты питания"}</div>
                            </td>
                            <td className={s.category_element} onClick={() => {this.state.data = "Медицина"; console.log(this.state.data)}}
                                style={{backgroundImage: `url(${icon3})`, backgroundSize: "cover", marginLeft: 48+"px"}}
                            >

                                <div className={s.category_element_name}>{"Медицина"}</div>
                            </td>

                        </tr>
                        <tr className={s.category_row}>
                            <td className={s.category_element} onClick={() => {this.state.data = "Бытовая техника"; console.log(this.state.data)}}
                                style={{backgroundImage: `url(${icon4})`, backgroundSize: "cover", marginLeft: "0"}}
                            >

                                <div className={s.category_element_name}>{"Бытовая техника"}</div>
                            </td>

                            <td className={s.category_element} onClick={() => {this.state.data = "Красота и гигиена"; console.log(this.state.data)}}
                                style={{backgroundImage: `url(${icon5})`, backgroundSize: "cover", marginLeft: 48+"px"}}
                            >

                                <div className={s.category_element_name}>{"Красота и гигиена"}</div>
                            </td>

                            <td className={s.category_element} onClick={() => {this.state.data = "Бытовая техника"; console.log(this.state.data)}}
                                style={{backgroundImage: `url(${icon6})`, backgroundSize: "cover", marginLeft: 48+"px"}}
                            >

                                <div className={s.category_element_name}>{"Бытовая химия"}</div>
                            </td>


                        </tr>
                    </table>
                    <div className={s.searching_form}>
                        <label className={s.searching_form_line}>
                            <input type="text" className={s.searching_form_line_input} id="search_text"
                                   onKeyDown={this.handle.bind(this)} placeholder="Поиск"/>
                            <img className={s.searching_form_line_icon} alt="Loop Icon"
                                 src="data:image/svg+xml;base64, PHN2ZyB3aWR0aD0iMTYiIGhlaWdodD0iMTYiIHZpZXdCb3g9IjAgMCAxNiAxNiIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPHBhdGggZD0iTTYuMDAwMDEgMTJDOS4zMDg0NiAxMiAxMiA5LjMwODQ2IDEyIDYuMDAwMDFDMTIgMi42OTE1NiA5LjMwODQzIDAgNi4wMDAwMSAwQzIuNjkxNTYgMCAwIDIuNjkxNTYgMCA2LjAwMDAxQzAgOS4zMDg0MyAyLjY5MTU2IDEyIDYuMDAwMDEgMTJaTTYuMDAwMDEgMS45Mzc1OEM4LjI0MDA0IDEuOTM3NTggMTAuMDYyNCAzLjc1OTk4IDEwLjA2MjQgNS45OTk5OEMxMC4wNjI0IDguMjQwMDQgOC4yNDAwNCAxMC4wNjI0IDYuMDAwMDEgMTAuMDYyNEMzLjc1OTk4IDEwLjA2MjQgMS45Mzc1OCA4LjI0MDA0IDEuOTM3NTggNS45OTk5OEMxLjkzNzU4IDMuNzU5OTggMy43NTk5OCAxLjkzNzU4IDYuMDAwMDEgMS45Mzc1OFoiIGZpbGw9IiM5OTk4OUUiLz4KPHBhdGggZD0iTTE0LjMxMjggMTZDMTQuNzYzNyAxNiAxNS4xODc0IDE1LjgyNDQgMTUuNTA1OCAxNS41MDU1QzE1LjgyNDQgMTUuMTg3MyAxNiAxNC43NjM3IDE2IDE0LjMxMjhDMTYgMTMuODYyIDE1LjgyNDQgMTMuNDM4MSAxNS41MDU2IDEzLjExOTNMMTIuMTY0NSA5Ljc3ODEzQzEyLjA3NTYgOS42ODkyOSAxMS45NjA5IDkuNjQwMjkgMTEuODQxNSA5LjY0MDI5QzExLjY5MzEgOS42NDAyOSAxMS41NTY1IDkuNzE0MTIgMTEuNDY2NyA5Ljg0Mjg3QzExLjAyNDQgMTAuNDc3OSAxMC40NzgxIDExLjAyNDMgOS44NDMwOSAxMS40NjY1QzkuNzI3MTIgMTEuNTQ3MyA5LjY1Mzg4IDExLjY2OTkgOS42NDIxMiAxMS44MDI5QzkuNjMwNDYgMTEuOTM0NCA5LjY4MDA2IDEyLjA2NjIgOS43NzgyNCAxMi4xNjQzTDEzLjExOTMgMTUuNTA1NUMxMy40MzgxIDE1LjgyNDQgMTMuODYyIDE2IDE0LjMxMjggMTZaIiBmaWxsPSIjOTk5ODlFIi8+Cjwvc3ZnPgo="/>
                        </label>

                    <SearchingForm/>
                    </div>
                    {this.state.loading || !this.state.items ? (
                        <div> Loading products...</div>
                    ) : (
                        <div>
                            {
                                this.state.items.map((product) => (
                                    <Products
                                        productName={product.product_name}
                                        productLink={product.product_link}
                                        productImg={product.product_img_link}
                                        productPrice={product.product_rub_price}
                                        productVendor={product.vendor_name}/>
                                ))
                            }
                        </div>
                    )
                    }


                </div>
            </div>
        )
    }
}

export default Content