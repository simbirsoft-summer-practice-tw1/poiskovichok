import './css/App.css';
import Content from "./pages/Content";
import Header from './components/Header';
import Footer from './components/Footer';
//import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
//import DropdownFilter from './components/DropdownFilter'

function App() {
  return (
    <div>
        <Header/>
        <Content/>
        <Footer/>
    </div>
  );
}


export default App;
