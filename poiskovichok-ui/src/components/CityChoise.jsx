import React from 'react';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import "../components/Header.module.css";
import {Button} from "@material-ui/core";

const options = [
    'Казань',
    'Москва',
    'Новосибирск',
    'Екатеринбург',
    'Самара',
    'Челябинск',
    'Омск',
    'Уфа',
    'Красноярск',
    'Пермь',
    'Волгоград',
    'Воронеж',
    'Саратов',
    'Краснодар',
    'Тольятти',
    'Тюмень',
    'Ижевск',
    'Барнаул',
    'Ульяновск',
    'Томск',
    'Астрахань',
    'Пенза',
    'Тула',
    'Сочи',
];

const ITEM_HEIGHT = 48;

export default function CityChoice() {
    const [anchorEl, setAnchorEl] = React.useState(null);
    const open = Boolean(anchorEl);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    return (
        <>
            <Button
                style={{ backgroundColor: 'transparent',backgroundImage:'' }}
                aria-label="more"
                aria-controls="long-menu"
                aria-haspopup="true"
                onClick={handleClick}
            >
                <h5 className="city_choice_text">Казань</h5>
            </Button>
            <Menu
                id="long-menu"
                anchorEl={anchorEl}
                keepMounted
                open={open}
                onClose={handleClose}
                PaperProps={{
                    style: {
                        maxHeight: ITEM_HEIGHT * 4.5,
                        width: '20ch',
                    },
                }}
            >
                {options.map((option) => (
                    <MenuItem key={option} selected={option === 'Казань'} onClick={handleClose}>
                        {option}
                    </MenuItem>
                ))}
            </Menu>
        </>
    );
}