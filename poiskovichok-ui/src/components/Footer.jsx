import React from "react";

class Footer extends React.Component{
    render() {
        return(
                <footer className="footer">
                    <div className="footer_logo">поисковичок 2021 ©</div>
                    <div className="footer_contact_info">
                        <div className="footer_contact_number">+1 (281) 330-8004</div>
                        <div className="footer_contact_email">support@poiskovichok.ru</div>
                    </div>
                </footer>
        )
    }
}
export default Footer