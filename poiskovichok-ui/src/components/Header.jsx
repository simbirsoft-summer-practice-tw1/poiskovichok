import React,{Component} from "react";
//import DropdownFilter from "./DropdownFilter";
import s from './Header.module.css';
import CityChoise from "./CityChoise";
const Header = () => {
        return(
                <header className={s.header}>
                    <div className={s.header_logo}>поисковичок</div>
                    <div className={s.city_choice}>
                        <img className={s.city_choice_img}
                             src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjQiIGhlaWdodD0iMjQiIHZpZXdCb3g9IjAgMCAyNCAyNCIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPHBhdGggZD0iTTEyLjIwMzUgMC43MzgyODFDMTYuNTE1OSAwLjczODI4MSAyMC4wMjQ0IDQuMjQ2NzYgMjAuMDI0NCA4LjU1OTE3QzIwLjAyNDQgMTMuOTc5NCAxMi4xOTU4IDIzLjIzODMgMTIuMTk1OCAyMy4yMzgzQzEyLjE5NTggMjMuMjM4MyA0LjM4MjYzIDEzLjcxMjkgNC4zODI2MyA4LjU1OTE3QzQuMzgyNjMgNC4yNDY3NiA3Ljg5MDk4IDAuNzM4MjgxIDEyLjIwMzUgMC43MzgyODFaTTkuODQzOCAxMC44NDkxQzEwLjQ5NDUgMTEuNDk5NyAxMS4zNDg5IDExLjgyNSAxMi4yMDM1IDExLjgyNUMxMy4wNTggMTEuODI1IDEzLjkxMjcgMTEuNDk5NyAxNC41NjMxIDEwLjg0OTFDMTUuODY0MyA5LjU0ODA4IDE1Ljg2NDMgNy40MzEwMSAxNC41NjMxIDYuMTI5ODJDMTMuOTMzIDUuNDk5NDggMTMuMDk0OSA1LjE1MjMxIDEyLjIwMzUgNS4xNTIzMUMxMS4zMTIxIDUuMTUyMzEgMTAuNDc0MSA1LjQ5OTYyIDkuODQzOCA2LjEyOTgyQzguNTQyNiA3LjQzMTAxIDguNTQyNiA5LjU0ODA4IDkuODQzOCAxMC44NDkxWiIgZmlsbD0iYmxhY2siLz4KPC9zdmc+Cg=="
                             alt="Geo point"/>
                        <CityChoise/>
                    </div>

                </header>
        )
}
export default Header