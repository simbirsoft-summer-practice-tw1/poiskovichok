import React from 'react';
import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

const options = [
    'По возрастанию цены',
    'По убыванию цены',
    'По популярности',
];

const ITEM_HEIGHT = 48;

export default function FilterList() {
    const [anchorEl, setAnchorEl] = React.useState(null);
    const open = Boolean(anchorEl);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    return (
        <div className="searching_form_sort_button">
            <IconButton
                aria-label="more"
                aria-controls="long-menu"
                aria-haspopup="true"
                onClick={handleClick}
            >
                <img className="searching_form_sort_button_icon" alt="Sort Button"
                     src="data:image/svg+xml;base64, PHN2ZyB3aWR0aD0iMTYiIGhlaWdodD0iMTYiIHZpZXdCb3g9IjAgMCAxNiAxNiIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPHBhdGggZD0iTTIuOTA5MTUgNi41NDUzOUgxMy4wOTA4QzEzLjI4NzggNi41NDUzOSAxMy40NTgzIDYuNDczNDcgMTMuNjAyMiA2LjMyOTVDMTMuNzQ2IDYuMTg1NTggMTMuODE4MiA2LjAxNTE1IDEzLjgxODIgNS44MTgxOEMxMy44MTgyIDUuNjIxMiAxMy43NDYgNS40NTA4NSAxMy42MDIyIDUuMzA2NzNMOC41MTEzMyAwLjIxNTg4MkM4LjM2NzQ5IDAuMDcyMTE5OCA4LjE5NzEgMCA4IDBDNy44MDI5MSAwIDcuNjMyNTIgMC4wNzIxMTk4IDcuNDg4NTYgMC4yMTU4ODJMMi4zOTc3MSA1LjMwNjczQzIuMjUzNzQgNS40NTA2OSAyLjE4MTgyIDUuNjIxMiAyLjE4MTgyIDUuODE4MThDMi4xODE4MiA2LjAxNTExIDIuMjUzNzQgNi4xODU1OCAyLjM5NzcxIDYuMzI5NUMyLjU0MTgzIDYuNDczNDcgMi43MTIyMSA2LjU0NTM5IDIuOTA5MTUgNi41NDUzOVoiIGZpbGw9IiM5OTk4OUUiLz4KPHBhdGggZD0iTTEzLjA5MDggOS40NTQ3M0gyLjkwOTE1QzIuNzEyMDYgOS40NTQ3MyAyLjU0MTY3IDkuNTI2NjkgMi4zOTc3MSA5LjY3MDQ5QzIuMjUzNzQgOS44MTQ0NSAyLjE4MTgyIDkuOTg0ODQgMi4xODE4MiAxMC4xODE4QzIuMTgxODIgMTAuMzc4OCAyLjI1Mzc0IDEwLjU0OTMgMi4zOTc3MSAxMC42OTMyTDcuNDg4NTYgMTUuNzg0QzcuNjMyNjggMTUuOTI3OSA3LjgwMzA3IDE2IDggMTZDOC4xOTY5NCAxNiA4LjM2NzQ5IDE1LjkyNzkgOC41MTEzMyAxNS43ODRMMTMuNjAyMiAxMC42OTMxQzEzLjc0NiAxMC41NDkzIDEzLjgxODIgMTAuMzc4OCAxMy44MTgyIDEwLjE4MThDMTMuODE4MiA5Ljk4NDg0IDEzLjc0NiA5LjgxNDQ1IDEzLjYwMjIgOS42NzA0NUMxMy40NTgzIDkuNTI2NTMgMTMuMjg3OCA5LjQ1NDczIDEzLjA5MDggOS40NTQ3M1oiIGZpbGw9IiM5OTk4OUUiLz4KPC9zdmc+Cg=="/>
            </IconButton>
            <Menu
                id="long-menu"
                anchorEl={anchorEl}
                keepMounted
                open={open}
                onClose={handleClose}
                PaperProps={{
                    style: {
                        maxHeight: ITEM_HEIGHT * 4.5,
                        width: '30ch',
                    },
                }}
            >
                {options.map((option) => (
                    <MenuItem key={option} selected={option === 'По возрастанию'} onClick={handleClose}>
                        {option}
                    </MenuItem>
                ))}
            </Menu>
        </div>
    );
}
