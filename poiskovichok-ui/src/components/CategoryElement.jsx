import React from "react";
import s from './CategoryElement.module.css'

const CategoryElement = (props) => {
    //props.image ?
        return(
                <td className={s.category_element}
                    style={{backgroundImage: `url(${props.image})`, backgroundSize: props.backgroundSize, marginLeft: props.mLeft+"px",width: props.width+"%"}}>

                    <div className={s.category_element_name}>{props.name}</div>
                </td>
        )
}
export default CategoryElement;