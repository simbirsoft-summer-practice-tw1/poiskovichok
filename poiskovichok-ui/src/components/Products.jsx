import React from "react";
import s from "./Products.module.css";

const Products = (props) => {
   const goToProduct = async () => {
       const apiUrl = "http://127.0.0.1:8000/api/products/method_c/"
       const response = await fetch(
           apiUrl,
           {
               method: "POST",
               body: JSON.stringify({"product_name":props.productName,"vendor_name":props.productVendor,"product_link":props.productLink,"product_img_link":props.productImg,"product_rub_price":props.productPrice}),
               headers: {"Content-type": "application/json"}
           }
       );
       console.log(" Method C ")
       console.log(response);
   }
    return(
            <div className="products">
                <div className={s.product}>
                    <div className={s.product_image_wrapper}>
                        <img className={s.product_image}
                             src={props.productImg}
                             alt="Product Image"/>
                    </div>
                    <div className={s.product_info}>
                        <div className={s.product_vendor}>{props.productVendor}</div>
                        <div className={s.product_info_row_1}>
                            <a href={props.productLink} onClick={goToProduct} target="_blank" className={s.product_name}>{props.productName}</a>
                            <div className={s.product_price}>₽ {props.productPrice}</div>
                        </div>
                        <div className={s.product_info_row_2} >
                            <div className={s.product_nav_element} ><a>отзывы</a></div>
                            <div className={s.product_nav_element} ><a>показать на карте</a></div>
                        </div>
                    </div>
                </div>
            </div>
    )
}

export default Products;